# Sortable Recipes

A sample project for my EmberConf 2020 talk. Leverages Ember Octane and is fully test driven. Includes the RevealJS presentation and a simple Python backend.

[![](https://gaiety.life/images/uploads/a11yfirst.png) Watch the talk here](https://gaiety.life/a11y-first-and-everyone-wins)

## Getting Started

* [Set up and run the presentation](./presentation/README.md)
* [Set up and run the backend](./python-api/README.md)
* [Set up and run the frontend](./ember-ui/README.md)
* [Set up and run the frontend (modern)](./ember-ui-modern/README.md)

