import ApplicationAdapter from './application';

export default class MealAdapter extends ApplicationAdapter {
  async findRecord(store, model, id) {
    let result = await fetch(`${this.host}meal/${id}`);
    return await result.json();
  }

  async queryRecord(store, model, query) {
    if (query === 'random') {
      let result = await fetch(`${this.host}meals/random`);
      return await result.json();
    }
  }
}
