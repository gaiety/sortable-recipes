import Component from '@glimmer/component';
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class HeaderNavComponent extends Component {
  @tracked hideLinks = true;

  @action
  toggle() {
    this.hideLinks = !this.hideLinks;
  }
}
