import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import { action } from "@ember/object";

export default class MealAddComponent extends Component {
  @service store;

  @(task(function * () {
    return yield this.store.queryRecord('meal', 'random');
  })) fetchMeal;

  @action
  async addMeal() {
    await this.fetchMeal.perform();
  }
}
