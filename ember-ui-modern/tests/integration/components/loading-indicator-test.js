import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | loading-indicator', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`<LoadingIndicator />`);

    assert.dom('[data-test-id=loading-indicator]').hasTagName('svg');
    assert.dom('[data-test-id=loading-indicator] animate').exists();
  });
});
