import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | meal', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('meal', {});

    assert.ok(model);
  });

  test('it creates comma separated ingredients list', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('meal', {
      ingredients: [
        {
          name: 'Love',
          measure: 'Infinity',
        }, {
          name: 'Chocolate',
          measure: 'As Much As You Desire',
        },
      ],
    });

    assert.equal(model.ingredientsList, 'Love, Chocolate');
  });
});
