import Controller from '@ember/controller';
import { tracked } from "@glimmer/tracking";
import { A } from '@ember/array';
import { inject as service } from '@ember/service';

export default class IndexController extends Controller {
  @service store;
  @tracked items = A([]);

  constructor() {
    super(...arguments);
    this.assignExistingItems();
  }

  async assignExistingItems() {
    let items = await this.store.peekAll('meal');
    this.items = items;
  }

  get userHasMeals() {
    return this.items.length >= 1;
  }
}
