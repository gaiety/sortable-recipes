import DS from 'ember-data';
const { Model, attr } = DS;

export default class MealModel extends Model {
  @attr alternateDrink;
  @attr area;
  @attr category;
  @attr dateModified;
  @attr ingredients;
  @attr instructions;
  @attr name;
  @attr sourceUrl;
  @attr tags;
  @attr thumbnailUrl;
  @attr youtubeUrl;

  @attr('number', { defaultValue: Infinity }) listOrder;

  get ingredientsList() {
    return this.ingredients.map(ingredient => ingredient.name).join(', ');
  }
}
