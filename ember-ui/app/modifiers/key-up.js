import { modifier } from 'ember-modifier';

export default modifier(function keyUp(element, [handler], { key: desiredKey }) {
  let keyupListener = (evt) => {
    if (!desiredKey || desiredKey === evt.key) {
      handler(evt);
    }
  }

  element.addEventListener('keyup', keyupListener);

  return () => {
    element.removeEventListener('keyup', keyupListener);
  }
});
