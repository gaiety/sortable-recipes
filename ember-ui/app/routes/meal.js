import Route from '@ember/routing/route';

export default class MealRoute extends Route {
  model({ id }) {
    return this.store.findRecord('meal', id);
  }
}
