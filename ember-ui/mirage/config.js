export default function() {
  this.urlPrefix = 'http://localhost:8000/';

  this.get('/meals/random', (schema) => {
    let id = 'random';
    schema.meals.create({id});
    return schema.meals.find(id);
  });

  this.get('/meal/:id');
}

