import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | header-nav-link', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders link with href', async function(assert) {
    await render(hbs`
      <HeaderNavLink @href="foo">
        bar
      </HeaderNavLink>
    `);

    assert.dom('[data-test-id=header-nav-link]').hasAttribute('href', 'foo');
    assert.dom('[data-test-id=header-nav-link]').includesText('bar');
  });
});
