import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | header-nav', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders branding', async function(assert) {
    await render(hbs`<HeaderNav />`);

    assert.dom('[data-test-id=branding-link]').hasTagName('a');
    assert.dom('[data-test-id=branding-link]').includesText('Sortable Recipes');
  });

  test('it renders fork link', async function(assert) {
    await render(hbs`<HeaderNav />`);

    assert.dom('[data-test-id=fork-link]').hasTagName('a');
    assert.dom('[data-test-id=fork-link]').includesText('Fork');
  });

  test('it renders links', async function(assert) {
    await render(hbs`<HeaderNav />`);

    assert.dom('[data-test-id=nav-links] a').exists();
  });

  test('it can toggle link visibility', async function(assert) {
    await render(hbs`<HeaderNav />`);

    assert.dom('[data-test-id=nav-links]').hasClass('hidden');

    await click('[data-test-id=toggle-menu]');
    assert.dom('[data-test-id=nav-links]').doesNotHaveClass('hidden');

    await click('[data-test-id=toggle-menu]');
    assert.dom('[data-test-id=nav-links]').hasClass('hidden');
  });
});
