import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | meal-add', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  test('it adds a new meal (meal) when button is clicked', async function(assert) {
    await render(hbs`<MealAdd />`);
    let store = this.owner.lookup('service:store');

    await click('[data-test-id=meal-add-button]');
    let randomMeal = await store.findRecord('meal', 'random');

    assert.ok(randomMeal);
    assert.equal(store.peekAll('meal').length, 1);
  });
});
