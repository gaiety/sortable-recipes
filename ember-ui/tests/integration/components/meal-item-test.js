import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | meal-item', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(initMeal);

  const defaultTemplate = hbs`
    <MealItem
      @meal={{meal}}
      @handleFocus={{handleFocus}}
      @handleBlur={{handleBlur}} />
  `;

  test('it renders a wrapper href', async function(assert) {
    await render(defaultTemplate);

    assert.dom('[data-test-id=meal-link]').hasTagName('a');
  });

  test('it renders meal preview image', async function(assert) {
    await render(defaultTemplate);

    assert.dom('[data-test-id=meal-preview-image]').hasAttribute('src', 'image.jpg');
  });

  test('it renders meal data as text', async function(assert) {
    await render(defaultTemplate);

    assert.dom('[data-test-id=meal-name]').hasText('Cookies');
    assert.dom('[data-test-id=meal-ingredients-list]').hasText('Love, Chocolate');
    assert.dom('[data-test-id=meal-tags]').includesText('Home');
    assert.dom('[data-test-id=meal-tags]').includesText('Desserts');
  });

  async function initMeal() {
    let store = this.owner.lookup('service:store');
    this.server.create('meal', {
      area: 'Home',
      category: 'Desserts',
      id: 1,
      ingredients: [
        {
          name: 'Love',
          measure: 'Infinity',
        }, {
          name: 'Chocolate',
          measure: 'As Much As You Desire',
        },
      ],
      name: 'Cookies',
      thumbnailUrl: 'image.jpg',
    });
    this.meal = await store.findRecord('meal', 1);
    this.handleFocus = () => {};
    this.handleBlur = () => {};
  }
});
