import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | meal-tag', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders with hashtag', async function(assert) {
    await render(hbs`
      <MealTag>foo</MealTag>
    `);

    assert.dom('[data-test-id="meal-tag"]').includesText('#foo');
  });
});
