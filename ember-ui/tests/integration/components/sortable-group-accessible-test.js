import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, find, click, triggerEvent, triggerKeyEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | sortable-group-accessible', function(hooks) {
  setupRenderingTest(hooks);

  test('it can yield text content', async function(assert) {
    await render(hbs`
      <SortableGroupAccessible>
        template block text
      </SortableGroupAccessible>
    `);

    assert.equal(this.element.textContent.trim(), 'template block text');
  });

  test('it can pass splattributes to wrapper div', async function(assert) {
    await render(hbs`
      <SortableGroupAccessible
        class='foo'
        data-test-id='wrapper' />
    `);

    assert.dom('[data-test-id=wrapper]').hasClass('foo');
  });

  test('it has a wrapper div with a unique id', async function(assert) {
    await render(hbs`
      <SortableGroupAccessible data-test-id='first' />
      <SortableGroupAccessible data-test-id='second' />
    `);

    assert.dom('[data-test-id=first]').hasAttribute('data-id');
    assert.dom('[data-test-id=second]').hasAttribute('data-id');
    assert.notEqual(find('[data-test-id=first]').getAttribute('data-id'), find('[data-test-id=second]').getAttribute('data-id'));
  });

  test('it can yield whether it has group focus or not', async function(assert) {
    await render(hbs`
      <SortableGroupAccessible
        tabindex="0"
        data-test-id='wrapper'
        as | groupHasFocus |>
        <span data-test-id='hasFocusWrapper'>{{groupHasFocus}}</span>
      </SortableGroupAccessible>
    `);

    assert.dom('[data-test-id=hasFocusWrapper]').hasText('false');

    await triggerEvent('[data-test-id=wrapper]', 'focus');
    assert.dom('[data-test-id=hasFocusWrapper]').hasText('true');

    await triggerEvent('[data-test-id=wrapper]', 'blur');
    assert.dom('[data-test-id=hasFocusWrapper]').hasText('false');
  });

  test('it can yield focus action', async function(assert) {
    await render(hbs`
      <SortableGroupAccessible
        tabindex="0"
        data-test-id='wrapper'
        as | groupHasFocus _ groupActions |>
        <span data-test-id='hasFocusWrapper'>{{groupHasFocus}}</span>
        <button role="button" data-test-id='focusButton' {{action groupActions.handleFocus}}>Focus</button>
      </SortableGroupAccessible>
    `);

    await click('[data-test-id=focusButton]');
    assert.dom('[data-test-id=hasFocusWrapper]').hasText('true');
  });

  test('it yields selected index while arrow keying up/down', async function(assert) {
    this.models = ['foo', 'bar'];

    await render(hbs`
      <SortableGroupAccessible
        tabindex="0"
        data-test-id='wrapper'
        @models={{models}}
        as | groupHasFocus selectedIndex |>
        <span data-test-id='selectedIndexWrapper'>{{selectedIndex}}</span>
      </SortableGroupAccessible>
    `);

    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('0');

    await triggerEvent('[data-test-id=wrapper]', 'focus');
    await triggerKeyEvent('[data-test-id=wrapper]', 'keyup', 'ArrowDown');
    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('1', 'Can select down');

    await triggerKeyEvent('[data-test-id=wrapper]', 'keyup', 'ArrowDown');
    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('1', 'Does not extend past array length');

    await triggerKeyEvent('[data-test-id=wrapper]', 'keyup', 'ArrowUp');
    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('0', 'Can select up');

    await triggerKeyEvent('[data-test-id=wrapper]', 'keyup', 'ArrowUp');
    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('0', 'Can select up');

    await triggerKeyEvent('[data-test-id=wrapper]', 'keyup', 'ArrowUp');
    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('0', 'Does not extend beyond 0');
  });

  test('it yields selected index while arrow keying up/down', async function(assert) {
    this.models = ['foo', 'bar'];

    await render(hbs`
      <SortableGroupAccessible
        tabindex="0"
        data-test-id='wrapper'
        @models={{models}}
        as | groupHasFocus selectedIndex |>
        <span data-test-id='selectedIndexWrapper'>{{selectedIndex}}</span>
      </SortableGroupAccessible>
    `);

    assert.dom('[data-test-id=selectedIndexWrapper]').hasText('0');
  });
});
