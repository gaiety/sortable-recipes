import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | sortable-item-accessible', function(hooks) {
  setupRenderingTest(hooks);

  test('it can yield text', async function(assert) {
    await render(hbs`
      <SortableItemAccessible>
        yielded text
      </SortableItemAccessible>
    `);

    assert.equal(this.element.textContent.trim(), 'yielded text');
  });

  test('it does not yield sort buttons by default', async function(assert) {
    await render(hbs`<SortableItemAccessible />`);

    assert.dom('[data-test-id=sort-up]').doesNotExist();
    assert.dom('[data-test-id=sort-down]').doesNotExist();
  });

  test('it does not yield sort buttons if group does not have focus', async function(assert) {
    await render(hbs`
      <SortableItemAccessible
        @currentIndex={{0}}
        @selectedIndex={{0}} />
    `);

    assert.dom('[data-test-id=sort-up]').doesNotExist();
    assert.dom('[data-test-id=sort-down]').doesNotExist();
  });

  test('it does not yield sort buttons if indices dont match', async function(assert) {
    await render(hbs`
      <SortableItemAccessible
        @groupHasFocus={{true}}
        @currentIndex={{0}}
        @selectedIndex={{1}} />
    `);

    assert.dom('[data-test-id=sort-up]').doesNotExist();
    assert.dom('[data-test-id=sort-down]').doesNotExist();
  });

  test('it shows sort buttons when group has focus and indicies match', async function(assert) {
    this.mockFunction = () => {};

    await render(hbs`
      <SortableItemAccessible
        @groupHasFocus={{true}}
        @currentIndex={{0}}
        @selectedIndex={{0}}
        @orderItemUp={{this.mockFunction}}
        @orderItemDown={{this.mockFunction}}
        @handleBlur={{this.mockFunction}} />
    `);

    assert.dom('[data-test-id=sort-up]').exists();
    assert.dom('[data-test-id=sort-down]').exists();
  });

  test('it handles actions', async function(assert) {
    this.mockUp = () => { assert.step('up'); };
    this.mockDown = () => { assert.step('down'); };
    this.mockBlur = () => { assert.step('blur'); };

    await render(hbs`
      <SortableItemAccessible
        @groupHasFocus={{true}}
        @currentIndex={{0}}
        @selectedIndex={{0}}
        @orderItemUp={{this.mockUp}}
        @orderItemDown={{this.mockDown}}
        @handleBlur={{this.mockBlur}} />
    `);

    await click('[data-test-id=sort-up]');
    await click('[data-test-id=sort-down]');

    assert.verifySteps(['up', 'blur', 'down']);
  });
});
