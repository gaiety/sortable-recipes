import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, triggerKeyEvent } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { set } from '@ember/object';

module('Integration | Modifier | key-down', function(hooks) {
  setupRenderingTest(hooks);

  test('it fires off a function when a key down, passing the event along with it', async function(assert) {
    set(this, 'keyDown', ({ key }) => {
      assert.step('key down');
      assert.equal(key, 'Enter');
    });

    await render(hbs`
      <div {{key-down this.keyDown}}
        data-test-id='keydown'>
      </div>
    `);
    await triggerKeyEvent('[data-test-id=keydown]', 'keydown', 'Enter');

    assert.verifySteps(['key down']);
  });

  test('it can listen for a specific key', async function(assert) {
    set(this, 'keyDown', ({ key }) => {
      assert.step('enter key down');
      assert.equal(key, 'Enter');
    });

    await render(hbs`
      <div {{key-down this.keyDown key="Enter"}}
        data-test-id='keydown'>
      </div>
    `);
    await triggerKeyEvent('[data-test-id=keydown]', 'keydown', 'Enter');
    await triggerKeyEvent('[data-test-id=keydown]', 'keydown', 'Spacebar');

    assert.verifySteps(['enter key down']);
  });
});
