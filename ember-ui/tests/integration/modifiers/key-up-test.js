import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, triggerKeyEvent } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Modifier | key-up', function(hooks) {
  setupRenderingTest(hooks);

  test('it fires off a function when a key is pressed, passing the event along with it', async function(assert) {
    this.keyUp = ({ key }) => {
      assert.step('key up');
      assert.equal(key, 'Enter');
    };

    await render(hbs`
      <div {{key-up this.keyUp}}
        data-test-id='keyup'>
      </div>
    `);
    await triggerKeyEvent('[data-test-id=keyup]', 'keyup', 'Enter');

    assert.verifySteps(['key up']);
  });

  test('it can listen for a specific key', async function(assert) {
    this.keyUp = ({ key }) => {
      assert.step('enter key up');
      assert.equal(key, 'Enter');
    };

    await render(hbs`
      <div {{key-up this.keyUp key="Enter"}}
        data-test-id='keyup'>
      </div>
    `);
    await triggerKeyEvent('[data-test-id=keyup]', 'keyup', 'Enter');
    await triggerKeyEvent('[data-test-id=keyup]', 'keyup', 'Spacebar');

    assert.verifySteps(['enter key up']);
  });
});
