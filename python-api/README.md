# Python Backend for Sortable Recipes (EmberConf 2020)

## Getting Started

```bash
pip3 install requests requests-cache gunicorn pycnic
gunicorn api:app
```

### Available Endpoints:

`http://localhost:8000/meals/random`
`http://localhost:8000/meal/{meal_id}`
