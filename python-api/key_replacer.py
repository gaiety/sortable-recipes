class KeyReplacer():
    def __init__(self, replacements = []):
        self.replacements = self._build_replacements(replacements)

    def _build_replacements(self, replacements):
        return list(map(lambda r: { "old": r[0], "new": r[1] }, replacements))

    def _rename(self, obj, old_key, new_key):
        obj[new_key] = obj[old_key]
        del obj[old_key]

    def do_replace(self, obj):
        for replacement in self.replacements:
            self._rename(obj, replacement["old"], replacement["new"])

